package main

import (
	"encoding/json"
	"fmt"
	"io"

	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

// sapretor used in csv file
const SAP = "|"

// page not found error status code
const READ_ALL_PAGES float64 = 1004

// class not found error status code
const READ_ALL_CLASS float64 = 1005

func main() {

	language := [1]string{"en_US"}

	const API = "https://api.spectrumsurveys.com/suppliers/v2/attributes?localization="

	const ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyODU0YTU0NGE3YWVhNTdmZmU1NmY2NCIsInVzcl9pZCI6IjExNzU5IiwiaWF0IjoxNjUyOTAyNDg0fQ.MTXc3QZ-xORRDOi3Nz0I6gEbN_LpMH9m5Y01JkjgaDU"

	Answers_Data := "sep=" + SAP + "\n"
	Answers_Data += "qualification_code|text|desc|type|class|answer_id|answer_text\n"
	for _, lang := range language {
		// fmt.Println(lang)

		isValidClass := true
		for class := 1; isValidClass; class++ {

			for pageno := 1; ; pageno++ {

				url := API + lang + "&class=" + strconv.Itoa(class) + "&page=" + strconv.Itoa(pageno)
				// fmt.Println("class:", class, ", Page: ", pageno, ", URL:", url)

				spaceClient := http.Client{
					Timeout: time.Second * 60, // Timeout after 2 seconds
				}

				req, err := http.NewRequest(http.MethodGet, url, nil)
				if err != nil {
					log.Fatal(err)
				}

				//Access Token Added in Header
				req.Header.Set("access-token", ACCESS_TOKEN)

				res, getErr := spaceClient.Do(req)
				if getErr != nil {
					log.Fatal(getErr)
				}

				if res.Body != nil {
					defer res.Body.Close()
				}

				//Read Data of Body from Response
				content, readErr := io.ReadAll(res.Body)

				if readErr != nil {
					log.Fatal(readErr)
				}

				var rawMsg json.RawMessage
				err = json.Unmarshal(content, &rawMsg)

				if err != nil {
					fmt.Println("Error:", err)
					return
				}

				//Parse JSON Data using MAP instead of creating object
				var obj map[string]interface{}
				err = json.Unmarshal(rawMsg, &obj)

				if err != nil {
					fmt.Println("Error:", err)
					return
				}
				//check for token valid or not
				if obj["status"] != nil && obj["status"].(string) == "Failure" && obj["msg"] != nil && obj["msg"].(string) == "Invalid token" {
					log.Fatal(obj["msg"].(string))
					return
				}

				//Invalid Page Found
				if obj["statusCode"] != nil {
					if obj["statusCode"].(float64) == READ_ALL_CLASS {
						isValidClass = false
						break
					}

					if obj["statusCode"].(float64) == READ_ALL_PAGES {
						break
					}

				}

				for _, value := range obj["qual_attributes"].([]interface{}) {
					entry := value.(map[string]interface{})

					for _, value2 := range entry["condition_codes"].([]interface{}) {
						entry2 := value2.(map[string]interface{})
						Answers_Data += fmt.Sprint(entry["qualification_code"].(float64)) + SAP + entry["text"].(string) + SAP +
							entry["desc"].(string) + SAP +
							fmt.Sprint(entry["type"].(float64)) + SAP +
							fmt.Sprint(entry["class"].(float64)) + SAP +
							entry2["id"].(string) + SAP + entry2["text"].(string) + "\n"
					}

				}

			}
		}
	}

	newfile, file_creation_err_2 := os.Create("qcode_answerId_answerText.csv")

	if file_creation_err_2 != nil {
		log.Fatal(file_creation_err_2)
		return
	}

	newfile.WriteString(Answers_Data)
}
